

# %%

import os
import sys

# notebook imports
# we inject module path while developing
sys.path.insert(0, os.path.abspath('../src'))
import flogging  # pylint: disable=import-error
import pointy  # pylint: disable=import-error

