

# %% import modules
import sys
import os
import pcl
import numpy as np

# notebook imports
# we inject module path while developing
sys.path.insert(0, os.path.abspath('../src'))
import flogging  # pylint: disable=import-error
import points  # pylint: disable=import-error


# use a root logger to collect logs from submodules
# that also uses the root logger
flog = flogging.logger()
flog.level(flogging.TRACE)

raw_dir = os.path.join('..', 'data', 'raw')


# %% load data

depth_dir = os.path.join(raw_dir, 'scan1', 'scan1.npy')
depth = np.load(depth_dir)

flog.info('mapped depth array of shape: {} ({})'.format(depth.shape, depth.dtype))

# %% 

print(depth.min(), depth.max())
print(np.count_nonzero(~np.isnan(depth)))
print(np.count_nonzero(depth))


# %%

batch_dir = 'scan1'
filename = 'scan1_xyzrgba.pcd'
cloud_dir = os.path.join(raw_dir, batch_dir, filename)
cloud = points.load_point_cloud(cloud_dir)

# %%
## %%time

def frange(start, stop, step=1.0):
    ''' floating point range generator '''
    while start < stop:
        yield start
        start += step

dataset = cloud._obj.to_array()

dataset_z = dataset[[0]]
# height filtering from ground
zfiltered = (dataset[:, 2].max() - dataset[:, 2].min()) / 10.0

flog.trace('zfiltered: {}'.format(zfiltered))

grid_step = 100.0

xstep = (dataset[:, 0].max() - dataset[:, 0].min()) / grid_step
ystep = (dataset[:, 1].max() - dataset[:, 1].min()) / grid_step

for x in frange(dataset[:, 0].min(), dataset[:, 0].max(), grid_step):
    for y in frange(dataset[:, 1].min(), dataset[:, 1].max(), grid_step):

        reduced = dataset[
            (dataset[:, 0] > x)
            &(dataset[:, 0] < x + xstep)
            &(dataset[:, 1] > y)
            &(dataset[:, 1] < y + ystep)]

        if reduced.shape[0] > 0:
            filtered = reduced[
                reduced[:, 2] > (reduced[:, 2].min() + zfiltered)]

            if filtered.shape[0] > 0:
                dataset_z = np.concatenate(
                    (dataset_z, filtered))

flog.trace('dataset_z shape: {}'.format(dataset_z.shape))

# should take about 3min

# %% 

flog.info('z range: {}'.format(dataset_z[:, 2].max() - dataset_z[:, 2].min()))
flog.info('z max: {}, z min: {}'.format(dataset_z[:, 2].max(), dataset_z[:, 2].min()))
flog.info('x range: {}'.format(dataset_z[:, 0].max() - dataset_z[:, 0].min()))
flog.info('x max: {}, x min: {}'.format(dataset_z[:, 0].max(), dataset_z[:, 0].min()))
flog.info('y range: {}'.format(dataset_z[:, 1].max() - dataset_z[:, 1].min()))
flog.info('y max: {}, y min: {}'.format(dataset_z[:, 1].max(), dataset_z[:, 1].min()))

# %% 

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.addsubplot(111, projection='3d')


