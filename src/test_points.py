'''
Combined test cases for points library.
'''
# %%

import os
import pointy

def test_loading_point_cloud():
    ''' scripts must be able to read a scanned point cloud '''

    raw_dir = os.path.join('..', 'data', 'raw')

    cloud_dir = os.path.join(raw_dir, 'scan1', 'scan1_xyzrgba.ply')
    cloud = pointy.read(cloud_dir)

    assert cloud != None

# %%


def test_visualisation():
    ''' '''
    pass


# %%

if __name__ == "__main__":
    test_loading_point_cloud()

# %% examples

import os
import pointy
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from matplotlib.ticker import FormatStrFormatter

# %% 

raw_dir = os.path.join('..', 'data', 'raw')

cloud_dir = os.path.join(raw_dir, 'scan1', 'scan1_xyzrgba.ply')
cloud = pointy.read(cloud_dir)



# %% 

xyz = np.array(cloud.data.points)

X,Y = np.meshgrid(xyz[:,0], xyz[:,1])
Z = xyz[:,2]

# %%

print (xyz.shape)

# %%

fig = plt.figure()
ax = fig.gca(projection='3d')

# surf = ax.plot_surface(xyz[:,0], xyz[:,1], xyz[:,2], cmap=cm.coolwarm, linewidth=0, antialiased=False)
surf = ax.plot_surface(X, Y, Z, 'gray')

plt.show()

# %%




