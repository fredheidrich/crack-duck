''' A test file for  '''

# standard lib imports
import sys
import contextlib  # redirect_stdout py3.4+
from io import StringIO

# package imports
import pytest

# module imports
import flogging


# -----------------------------------------------------------------------------
# Helper

# xxx don't need this anymore in py3.4+
# also we can write our logs directly to a new stream now
# instead of diverting the deafult one /fred
@contextlib.contextmanager
def _redirect_stdout(out):
    '''
    Helper context manager to capture a string stream `out`
    Sets a `out` to be sys.stdout temporarily and restores it
    ad scope exit.
    found at so:
    https://stackoverflow.com/questions/4219717/how-to-assert-output-with-nosetest-unittest-in-python
    '''
    # store output
    prev = sys.stdout
    try:
        sys.stdout = out
        yield sys.stdout
    finally:
        # restore output
        sys.stdout = prev


# -----------------------------------------------------------------------------
# Tests


def test_version():
    ''' the flogging module shall include independent versioning '''
    assert flogging.__version__ != '', 'The version string shall not be empty'
    assert isinstance(flogging.__version__, str), 'Versioning must be a string'


def test_factory():
    ''' there should only be one logger of the same name '''
    logger1 = flogging.logger('foo')
    logger2 = flogging.logger('foo')

    assert id(logger1) == id(logger2), 'Creating two loggers with the same name should only create one logger'

    logger3 = flogging.logger('bar')
    assert id(logger1) != id(logger3), 'Using a different name should create a new logger'


def test_interface():
    ''' logger must support minimal logging facilities '''
    flog = flogging.logger('test.interface')

    out = StringIO()
    delegate = flogging.StreamOutputDelegate(out)
    flog.delegates = [delegate]
    test1_str = 'test'

    flog.info(test1_str)

    captured = out.getvalue().strip()
    assert captured.rfind(test1_str)


def test_levels():
    ''' logger must support setting different levels of logging 
    
    where log levels are an order of hierarchy
    CRITICAL > WARNING > INFO > DEBUG > TRACE

    runtime vs coding errors?
    error types?
    '''
    flog = flogging.logger('test.levels')
    flog.level = flogging.INFO  # should be default

    out = StringIO()
    test_str = 'test'
    delegate = flogging.StreamOutputDelegate(out)
    flog.delegates = [delegate]

    flog.debug(test_str)

    captured = out.getvalue().strip()
    assert captured == '', 'No output should have been captured but got {}'.format(captured)

    flog.level = flogging.DEBUG
    flog.debug(test_str)

    assert captured.rfind(test_str)

    # level must be retrievable
    assert flog.level == flogging.DEBUG

    # levels must be specified as integers
    with pytest.raises(TypeError) as exc_info:
        flog.level = 'INFO'
    assert 'must be an integer' in str(exc_info.value)


def test_interface_levels():
    ''' logger interface must provide handy shortcuts to
    basic logging facilities '''

    # creates a new string stream
    out = StringIO()

    flog = flogging.logger('test.interface_levels')
    # use our new string stream as delegate
    # for printing messages to
    delegate = flogging.StreamOutputDelegate(out)
    flog.delegates = [delegate]
    
    flog.level = flogging.TRACE
    
    # shortcuts for
    flog.trace('blah')
    flog.debug('blah')
    flog.info('blah')
    flog.warning('blah')
    flog.critical('blah')

    # general logging for supplied level
    flog.log(flogging.INFO, 'blah')


def test_disabling_logger():
    ''' it should be possible to disable a logger so it doesnt emit anything '''
    
    out = StringIO()
    delegate = flogging.StreamOutputDelegate(out)

    flog = flogging.logger('test.basic')
    flog.delegates = [delegate]
    flog.level = flogging.TRACE

    flog.is_enabled = False
    flog.debug('blah')
    captured = out.getvalue().strip()
    assert captured == '', 'No outout should have been logged if logger is disabled'


def test_delegates():
    ''' log system must support different delegates '''
    
    # must be able to handle situation where no delegate is available
    flog = flogging.logger('test.delegates')
    flog.delegates = []

    # log system must warn the user at least once that delegates is missing
    # and therefore logs will not be delegated to any output
    out = StringIO()
    flogging.pipe_warnings()
    with contextlib.redirect_stderr(out):
        flog.info('lost in the void')
    
    # pytest hijacks stderr so this can't be captured in tests
    # captured = out.getvalue().strip()
    # assert 'UserWarning' in captured


if __name__ == "__main__":
    test_version()
    test_factory()
    test_interface()
    test_levels()
    test_interface_levels()
    test_disabling_logger()
    test_delegates()
