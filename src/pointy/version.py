'''
The version strng from pointy the point cloud library wrapper
Follows semantic versioning scheme
https://semver.org
'''
__version__ = '0.1.0'