'''
Pointy
======

Defines an interface into some point cloud library.

'''

# standard lib imports

# third part imports
# import pcl
import open3d as o3

# internal imports
from pointy.version import __version__
import flogging


def draw_geometry(point_cloud):
    ''' visualizes a `point_cloud` loaded from the pointy module '''
    # unused, but allows 3d projection in matplotlib
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator
    from matplotlib.ticker import FormatStrFormatter

    fig = plt.figure()
    ax = fig.gca(projection='3d')



# use a root logger
logger = flogging.logger()


class Open3d_PointCloud:
    ''' Represents a point cloud from Open3d '''

    # xxx
    # o3 has some strange import errors so we have to ignore them
    # to not fluster the editor
    # pylint: disable=no-member

    __slots__ = (
        '_path',
        'data'
    )

    def __init__(self, path: str):
        '''
        Tries to open a new point cloud for `path`.
        Args:
            path (str) : path the .ply file
        Raises:
            TypeError : if `path` is not a string
        '''
        if not isinstance(path, str):
            raise TypeError('path is not a string')

        self._path = path
        self.data = self._read(path)

    
    def _read(self, path):
        '''
        reads a point cloud from `path` using open3d
        Raises:
            ValueError : on unsupported file extensions
        Returns:
            Returns open3d.geometry.PointCloud
        '''
        if not path.endswith('.ply'):
            raise ValueError('file extensions of {} is not supported'.format(path))

        return o3.io.read_point_cloud(path)
        


# defines the class to create point clouds with
_point_cloud_factory = Open3d_PointCloud


class PointCloudLibrary:
    ''' an interface to an external or internal point 
    cloud library '''
    def __init__(self):
        pass
    def load(self, path):
        '''
        loads a point cloud from `path`

        Args:
            path (str): a path to a .ply file

        Returns:
            point_cloud: a point cloud object
        '''
        return _point_cloud_factory(path)


_lib = PointCloudLibrary()


def read(path, library=_lib):
    ''' '''
    return _lib.load(path)


# xxx
# trying without PCL for a bit /fred
# class PCL_PointCloud:
#     ''' represents a point cloud. Usually an interface to other point clouds '''
#     __slots__ = (
#         '_obj'  # internal object
#     )
#     def __init__(self, obj):
#         self._obj = obj

#     def __repr__(self):
#         return 'pcl point cloud: {}'.format(self._obj)


# class PCL:
#     ''' An interface into the pcl library (point cloud library) '''
#     def __init__(self):
#         pass

#     def load(self, path):
#         return PCL_PointCloud(pcl.load_XYZRGBA(path))


# # currently used library
# _lib = PCL()


# def load_point_cloud(path, format='pcd'):
#     ''' load a point cloud from a known file `format` '''

#     cloud = None
#     if format is 'pcd':
#         cloud = _lib.load(path)
#     else:
#         raise Exception('Unrecognised file format {}'.format(path))

#     logger.trace('loaded {} {}'.format(path, cloud))
#     return cloud
