'''
'''

import sys
import warnings

from flogging import INFO
from flogging.record import LogRecord


raise_exceptions = True


# the class to use to create log records
_record_factory = LogRecord


class _LoggerBase:
    ''' represents the basic functionaly of a logger '''
    __slots__ = (
        '_level',
        '_logger',
        '_channel',
        '_format',
        '_timestamp_format',
        'delegates',
        'parent',
        'propagate',
        'is_enabled'
    )
    def __init__(self,  channel, level=INFO):
        self._channel = channel
        self._level = level
        self.parent = None
        self.propagate = True
        self.is_enabled = True
        self.delegates = []
        self._format = '{} {} {}: {}'
        self._timestamp_format = '%d/%m/%Y %H:%M:%S.%f'

    # -------------------------------------------------------------------------
    # public

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        if not isinstance(value, int):
            raise TypeError('level must be an integer')

        self._level = value

    # -------------------------------------------------------------------------
    # private
    
    def _is_enabled_for(self, level):
        ''' Check wether a logging `level` is enabled '''
        if not self.is_enabled:
            return False
        
        return True  # all other cases

    def _delegate(self, record):
        ''' call all other delegates with the log record '''
        # if filtering lets this log though then:
        self._call_delegates(record)

    def _call_delegates(self, record):
        ''' '''

        delegate = self
        count = 0
        while delegate:

            for d in self.delegates:
                count = count + 1
                if record.level >= d.level:
                    d.delegate(record)
            # xxx we currently don't have a hierarchy of logs
            if not delegate.propagate:
                delegate = None  # break
            else:
                delegate = delegate.parent
            
        if count == 0 and raise_exceptions:
            warnings.warn('No delegate were assigned to channel {}'.format(self._channel))


    def _make_record(self, name, level, msg, args, exc_info):
        ''' a factory method to create log records '''
        record = _record_factory(name, level, msg, args, exc_info)
        return record

    def _log(self, level, msg, args, exc_info=None):
        ''' logs a `msg` at the specified `level` '''

        # xxx could probably remove this because we assime
        # this is checked earlier
        # if not isinstance(level, int):
            # raise TypeError('level must be an integer')

        # execution info
        if exc_info:
            if isinstance(exc_info, BaseException):
                _exc_info = (type(exc_info), exc_info, exc_info.__traceback__)
            elif not isinstance(exc_info, tuple):
                _exc_info = sys.exc_info()
            else:
                _exc_info = None
        else:
            _exc_info = None

        record = self._make_record(self._channel, level, msg, args, _exc_info)
        self._delegate(record)
