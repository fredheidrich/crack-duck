'''
Contains the main logging delegate methods and classes
'''

import threading
import weakref
import atexit

from flogging.utils import thread_lock


# raise exceptions from the module itself
raise_exceptions = True

_delegates = weakref.WeakValueDictionary()
_delegate_list = []


def _deregister_delegate_ref(delegate_weakref):
    ''' helper function that deletes a delegate from the module delegate list '''
    with thread_lock():
        if delegate_weakref in _delegate_list:
            _delegate_list.remove(delegate_weakref)
        else: pass  # pragma: no cover


def _register_delegate_ref(delegate):
    ''' adds a reference to the delegate to the global list for
    neatly shutting down handlers '''    
    with thread_lock():
        _delegate_list.append(weakref.ref(delegate, _deregister_delegate_ref))


def _destroy(delegates=_delegate_list):  # pragma: no cover
    ''' cleanup all loggers and attempt to flush open buffers '''
    for ref in reversed(delegates):
        try:
            delegate = ref()
            try:
                with thread_lock(delegate.lock):
                    delegate.flush()
                    delegate.close()
            except (OSError, ValueError):
                pass  # ok because we're shutting down
        except:
            if raise_exceptions:
                raise
            else:
                pass  # ignore everything as we're shutting down

# runs shutdown procedure on program exit
atexit.register(_destroy)

class _BaseOutputDelegate:  # closable
    ''' An object that are meant to delegate log records
    to some delegate. '''

    def __init__(self):
        self.lock = threading.RLock()
        _register_delegate_ref(self)

    def emit(self, record):  # pragma: no cover
        raise NotImplementedError('emit must be implemented by subclass')

    def delegate(self, record):
        ''' '''
        with thread_lock(self.lock):
            self.emit(record)

    def flush(self):  # pragma: no cover
        ''' '''
        pass

    def close(self):  # pragma: no cover
        ''' must be implemented in subclasses that needs closing '''
        pass
