'''
Freds logging module (flogging)

Basically is just an interface into the python
standard logger

Flogging defines a LogRecord and works by defining
a set of delegates that handle the log record.

The logging backend consumes log records when requested

Heavily referenced by the python standard logger
https://github.com/python/cpython/blob/3.8/Lib/logging/__init__.py
'''

from flogging.version import __version__
from flogging.author import __author__
from flogging.record import LogRecord
from flogging.levels import (
    TRACE, DEBUG, INFO, WARNING, FATAL, CRITICAL
)
from flogging.stream import StreamOutputDelegate
from flogging.api import FLogging
from flogging.api import Logger  # class
from flogging.api import logger  # method
from flogging.api import pipe_warnings


__all__ = [
    'Logger', 'FLogging', 'logger', 'LogRecord',
    'pipe_warnings',
    'StreamOutputDelegate',
    'TRACE', 'DEBUG', 'INFO', 'WARNING', 'FATAL', 'CRITICAL'
]
