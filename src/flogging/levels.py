'''
Contains possible preset logging levels for the flogging module
'''


TRACE = 0  # print everything
DEBUG = 10
INFO = 20
WARNING = 30
CRITICAL = 40
FATAL = 50

_log_level_values = {
    'TRACE': TRACE,
    'DEBUG': DEBUG,
    'INFO': INFO,
    'WARNING': WARNING,
    'CRITICAL': CRITICAL,
    'FATAL': FATAL,
}
_log_level_names = {
    TRACE: 'trace',
    DEBUG: 'debug',
    INFO: 'info',
    WARNING: 'warning',
    CRITICAL: 'critical',
    FATAL: 'fatal',
}


def _level_to_str(level):
    ''' prints a string representation of `level` '''
    result = _log_level_names.get(level)
    if result is None:
        result = _log_level_values.get(level)

    return result