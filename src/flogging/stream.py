'''
Defines a stream delegate for log records
'''

import sys

from flogging.delegate import _BaseOutputDelegate
from flogging.format import Formatter
from flogging.levels import INFO
from flogging.utils import thread_lock

class StreamOutputDelegate(_BaseOutputDelegate):
    ''' Delegates log outputs to a stream. By default this is stderr '''
    __slots__ = (
        'stream',
        'formatter',
        'level',
        'terminator'  # line terminator
    )
    def __init__(self, stream=sys.stderr):
        super(StreamOutputDelegate, self).__init__()
        self.stream = stream
        self.formatter = Formatter()  # default
        self.level = INFO
        self.terminator = '\n'

    def flush(self):
        ''' flushes the stream '''
        with thread_lock(self.lock):
            self.stream.flush()

    def emit(self, record):
        ''' Emit a `record` to the output stream using a configured formatter. '''
        
        self.stream.write(
            record.format(self.formatter) + self.terminator)
        self.flush()
