'''
Contains base formatting and formatters for flogging module.
'''

class Formatter:
    ''' Converts formats into text '''
    
    __slots__ = (
        'fmt'
    )

    time_format = '%d/%m/%Y %H:%M:%S.%f'
    default_format = '{timestamp} {channel} {level}: {message}'

    def __init__(self, fmt=None):
        self.fmt = fmt or Formatter.default_format

    def format(self, record):
        ''' format `record` as text '''
        # Pass the formatter along and let the log
        # format itself to a string
        return record.format(self)
