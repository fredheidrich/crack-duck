'''
File contains the log record which records the information
on a single logged event
'''

import datetime

from flogging.levels import _level_to_str


class LogRecord:
    ''' An object representing a record of a single logged event '''
    __slots__ = (
        'channel',
        'msg',
        'level',
        'exc_info',
        'created',
        'args',
        '__dict__'
    )
    def __init__(self, channel, level, msg, args, exc_info):
        
        created = datetime.datetime.now()
        
        self.msg = msg
        self.level = level
        self.exc_info = exc_info
        self.created = created
        self.args = args

        # Dictionary describing the log content
        # note that it doesn't include all information
        # this is due that we need a formatter to format
        # the message and timestamp later
        self.__dict__ = {
            'channel': channel,
            'level': _level_to_str(level),
            'message': str(self),  # calls __str__
            # formatted on demand using `format`:
            'timestamp': ''
        }

    @property
    def message(self):
        return self.__dict__['message']
    
    @message.setter
    def message(self, value):
        self.__dict__['message'] = value

    def __repr__(self):
        ''' Print a string representation of the object for printing '''
        return '<LogRecord: {}>'.format(
            self.msg
        )

    def format(self, formatter):
        ''' Returns a formatted string of the record
        using supplied `formatter`. '''
        # set time using supplied formatter
        self.timestamp = self.created.strftime(formatter.time_format)

        # format log content using supplied formatter
        return formatter.fmt.format(**self.__dict__)

    def __str__(self):
        ''' returns a formatted string of this records message '''
        # format message using StringFormatter py3+
        if self.args:
            return str(self.msg).format(*self.args)
        else:
            return str(self.msg)
