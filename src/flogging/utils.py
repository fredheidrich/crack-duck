'''
Utility methods used by the module
'''


import contextlib
import threading


# The module level re-entrant lock
_lock = threading.RLock()


def single_instance(cls, *args, **kwargs):
    ''' maintains a single instance of class `cls` or creates 
    a new one if it doesn't exist already. Not to be confused
    with singleton pattern '''
    instances = {}
    def instance():
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return instance


@contextlib.contextmanager
def thread_lock(lock=_lock):
    ''' context manager that tries to acquire a provided `lock`
    on yield and finally releases it on scope closure.
    By default it uses a module scope lock '''
    try:
        yield lock.acquire()
    finally:
        lock.release()