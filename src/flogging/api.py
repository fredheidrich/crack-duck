'''
Contains interfaces to our flogging module.

These are the front facing modules a user will deal with.
'''

import warnings

from flogging.utils import thread_lock
from flogging.utils import single_instance
from flogging.logger import _LoggerBase
from flogging import (
    INFO, TRACE, DEBUG, WARNING, CRITICAL
)
from flogging.stream import StreamOutputDelegate


# default factory if no other delegate is selected
_default_delegate = StreamOutputDelegate


@single_instance
class FLogging():
    ''' Freds Logging facility aka flogger. Used as an interface
    for other loggers '''

    __slots__ = (
        'loggers'  # dictionary of loggers, with named keys
    )
    def __init__(self):
        self.loggers = {}

    def create(self, channel, level=INFO):
        ''' create a new logger or return existing of `channel`
        already exists '''

        with thread_lock():
            logger = self.loggers.get(channel, None)
            if logger is None:
                # create a new logger
                logger = Logger(channel, level)
                
                # set a new stream delegate
                delegate = _default_delegate()
                logger.delegates = [delegate]
                print(channel)

                # register logger
                self.loggers[channel] = logger
            
        return logger


class Logger(_LoggerBase):
    ''' An object representing a logger interface '''
    def __init__(self, channel, level=INFO):
        super(Logger, self).__init__(channel, level)

    def log(self, level, msg, *args, **kwargs):
        ''' '''
        if not isinstance(level, int):
            raise ValueError('level must be an integer')

        if self._is_enabled_for(level):
            self._log(level, msg, args, **kwargs)

    def trace(self, msg, *args, **kwargs):
        if self._is_enabled_for(TRACE):
            self._log(TRACE, msg, args, **kwargs)

    def debug(self, msg, *args, **kwargs):
        if self._is_enabled_for(DEBUG):
            self._log(DEBUG, msg, args, **kwargs)

    def info(self, msg, *args, **kwargs):
        ''' logs a message with the info logging level '''
        if self._is_enabled_for(INFO):
            self._log(INFO, msg, args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        if self._is_enabled_for(WARNING):
            self._log(WARNING, msg, args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        if self._is_enabled_for(CRITICAL):
            self._log(CRITICAL, msg, args, **kwargs)


def logger(name='root'):
    ''' create a new logger with the name of `name`
    if the name exists already return existing instance '''
    return FLogging().create(name)


_prev_warning_callback = None


def warning_callback(message, category, filename, lineno, file=None, line=None):
    ''' callback that gets call whenever a scripts emits a warning '''
    if file is not None:
        if _prev_warning_callback is not None:
            _prev_warning_callback(message, category, filename, lineno, file, line)  # pylint: disable=not-callable
    else:
        warning = warnings.formatwarning(message, category, filename, lineno, line)
        # create a new logger for warnings
        log = logger('py.warnings')
        # emit warning
        log.warning(warning)


def pipe_warnings(capture=True, callback=_prev_warning_callback):
    ''' Pipe standard warnigs to flogging module
    default == true '''
    if capture:
        # flip flop
        if callback is None:
            callback = warnings.showwarning
            warnings.showwarning = warning_callback
    else:
        if callback is not None:
            warnings.showwarning = callback
            callback = None
