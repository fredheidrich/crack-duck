3rd Party Library Versions
--------------------------

This software relies on third party libraries and applications.
The following versions and systems are tested and installed on the test machines.
See README.md for more information on specific installs.

## Verified

| Software     | Linux     | macOS              | Windows |
| ------------ | --------- | ------------------ | ------- |
| C++ Compiler | gcc 7.4.0 | Apple Clang 11.0.0 |         |
| CMake        | 3.10.2    | 3.16.2             |         |
| Python       | 3.6.9     | 3.7.4              |         |
| CUDA         | 10.2      |                    |         |

