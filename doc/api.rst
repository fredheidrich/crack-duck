
API
===

This part of the documentation list the full API reference for the module.

.. module:: flogging

Methods
-------

.. autofunction:: logger

Classes
-------

.. autoclass:: Logger
   :members:

.. autoclass:: LogRecord
   :members:

Delegates
---------

A delegate is an object that delegates the output string representation
of a `LogRecord` using a specified `Formatter`

.. autoclass:: StreamOutputDelegate
   :members:

Formatters
----------

A formatter takes a `LogRecord` and format its according to its' formatting
rules.

