.. Crack Duck documentation master file, created by
   sphinx-quickstart on Wed Jan 29 13:59:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. rst-class:: hide-header

Welcome to Crack Duck
=====================

Crack Duck is a python/C++ project intended to detect cracks in concrete
slabs using simultanous location and tracking (SLAM) as well as laser scanning.

It contains several submodules that has their own independent use.

Documentation
-------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

API Reference
-------------

.. toctree::
   :maxdepth: 2

   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
