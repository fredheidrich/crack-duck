Lainge O'Rourkes Crack Duck
===========================

A robotics system built to identify and measure cracks in relatively new concrete slab pours.

Supported Platforms
-------------------

The current system is currenly supported on Linux platforms and have been built and tested 
on Ubuntu 18.0.4.

Experimental support for macOS exists for development.

Dependencies
------------

The following dependecies are required:
  - C++ compiler
  - Python 3

The following dependencies are optional:
  - CUDA
  - Stereolabs ZED SDK

Building the Code
-----------------

#### 1. Install prerequisites (see [Dependencies](#dependencies) for required versions

  - Required:
    - C++ compiler:
      - gcc
    - CMake
    - Python

#### 2. Download source code

Download either source code archives from bitbucket [bitbucket](https://bitbucket.org) or
clone git repository.

```
% git clone ...
Cloning into 'crack-duck'
```


